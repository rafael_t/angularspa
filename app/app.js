/* global angular */

'use strict';

var app = angular.module('app', [
    'ngRoute', 
    'appControllers'
]);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/sites', {
            templateUrl: '/app/views/map.html',
            controller: 'SitesCtrl'
        })
        .when('/sites/:siteId', {
            templateUrl: '/app/views/map.html',
            controller: 'SiteDetailsCtrl'
        })
        .when('/assets/:assetId/history', {
            templateUrl: '/app/views/asset-history.html',
            controller: 'AssetHistoryCtrl'
        })
        .otherwise({
            redirectTo: '/sites'
        });
}]);
