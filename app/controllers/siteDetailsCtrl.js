/* jshint devel:true */
/* global angular, google, $, _ */

'use strict';

angular.module('appControllers')
    .controller('SiteDetailsCtrl', function($scope, $http, $routeParams) {

        var siteDetailsUrl = 'http://wellaware-frontend-dev-test.herokuapp.com/site/' + 
            $routeParams.siteId + '?callback=JSON_CALLBACK';

        $http.jsonp(siteDetailsUrl)
            .success(function(data) {
                processSiteDetails(data);
            })
            .error(function() {
                // TODO Oops
                console.error({mssg: 'Yikes!'});
            });

        function processSiteDetails(data) {

            var mapOptions = {
                zoom: 17,
                center: new google.maps.LatLng(data.latitude, data.longitude)
            };

            var infoWindow = new google.maps.InfoWindow();

            $scope.mapTitle = data.name + ' Assets Map';

            $scope.map = new google.maps.Map($('#map')[0], mapOptions);

            $scope.markers = [];

            $scope.assetDetails = {};

            _.each(data.endpoints, function(asset) {
                createMarker(asset, loadAssetDetails);
            });

            $scope.markerClick = function(e, marker) {
                e.preventDefault();
                google.maps.event.trigger(marker, 'click');
            };

            function createMarker(asset, clkCallback) {

                var marker = new google.maps.Marker({
                    id: asset.pk,
                    map: $scope.map,
                    position: new google.maps.LatLng(asset.latitude, asset.longitude),
                    title: asset.type + ': ' + asset.name
                });

                marker.content = $('#assetDetails')[0];

                google.maps.event.addListener(marker, 'click', clkCallback);

                $scope.markers.push(marker);
            }

            function loadAssetDetails() {
                var marker = this,
                    assetDetailsUrl = 'http://wellaware-frontend-dev-test.herokuapp.com/endpoint/' + 
                        marker.id + '?callback=JSON_CALLBACK';

                $http.jsonp(assetDetailsUrl)
                    .success(function() {
                        processAssetDetails(marker, data);
                    })
                    .error(function() {
                        // TODO Oops
                        console.error({mssg: 'Yikes!'});
                    });
            }

            function processAssetDetails(marker, data){
                $scope.assetDetails = {
                    id: data.pk,
                    title: data.name,
                    oilProd: data.today_oil_production,
                    gasProd: data.today_gas_production, 
                    waterProd: data.today_water_production
                };

                infoWindow.setContent(marker.content);
                infoWindow.open($scope.map, marker);
            }
        }

    });