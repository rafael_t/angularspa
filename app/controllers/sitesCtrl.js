/* jshint devel:true */
/* global angular, google, $, _ */

'use strict';

angular.module('appControllers')
    .controller('SitesCtrl', function($scope, $http, $rootScope, $location) {

        var sitesUrl = 'http://wellaware-frontend-dev-test.herokuapp.com/sites?callback=JSON_CALLBACK';

        $http.jsonp(sitesUrl)
            .success(function(data) {
                process(data.sites);
            })
            .error(function() {
                // TODO Oops
                console.error({mssg: 'Yikes!'});
            });

        function process(sites) {
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(29.604000, -98.53000)
            };

            $scope.mapTitle = 'Sites Map';

            $scope.map = new google.maps.Map($('#map')[0], mapOptions);

            $scope.markers = [];

            _.each(sites, function(site) {
                createMarker(site, loadSiteAssets);
            });

            $scope.markerClick = function(e, marker) {
                e.preventDefault();
                google.maps.event.trigger(marker, 'click');
            };

            function createMarker(site, clkCallback) {
                var marker = new google.maps.Marker({
                    id: site.pk,
                    map: $scope.map,
                    position: new google.maps.LatLng(site.latitude, site.longitude),
                    title: site.name
                });

                google.maps.event.addListener(marker, 'click', clkCallback);
                $scope.markers.push(marker);
            }

            function loadSiteAssets() {
                var url = '/sites/' + this.id;
                $rootScope.$apply(function(){ $location.path(url); });
            }
        }

    });