/* jshint devel:true */
/* global angular, $, _ */

'use strict';

angular.module('appControllers')
    .controller('AssetHistoryCtrl', function($scope, $routeParams, $http) {

        var assetHistoryUrl = 'http://wellaware-frontend-dev-test.herokuapp.com/endpoint/' +
            $routeParams.assetId + '/data' + '?callback=JSON_CALLBACK',

            seriesCfg = {
                oil_production: {label: 'Oil', order: 2},
                gas_production: {label: 'Gas', order: 1},
                water_production: {label: 'Water', order: 0}
            };

        $http.jsonp(assetHistoryUrl)
            .success(function(data) {
                processAssetHistory(data);
            })
            .error(function() {
                // TODO Oops
                console.error({mssg: 'Yikes!'});
            });

        function processAssetHistory(data) {
            var now = Date.today().setTimeToNow(),

                sampleCount = _.max(_.map(data, function(d) {
                    return _.size(d);
                })),

                series = _.map(data, function(v, k) {
                    return {
                        name: seriesCfg[k].label,
                        data: prepSamplesData(v, sampleCount),
                        order: seriesCfg[k].order
                    };
                }),

                categories = _.times(sampleCount, function() {
                    return now.addSeconds(-30).toString('HH:mm:ss');
                }).reverse();

            $(function() {
                $('#chart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Asset Production History'
                    },
                    xAxis: {
                        type: 'category',
                        categories: categories,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: 'Samples',
                            margin: 10
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Production'
                        }
                    },
                    tooltip: {
                        formatter: function() {
                            return '<em>' + this.x + '</em><br/>' +
                                this.series.name + ': ' + this.y + '<br/>' +
                                'Total: ' + this.point.stackTotal;
                        }
                    },
                    legend: {
                        verticalAlign: 'top',
                        y: 25,
                        floating: true,
                        backgroundColor: 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal'
                        }
                    },
                    series: series.sort(function(a, b){ return a.order - b.order; })
                });
            });
        }

        // right-pad (if incomplete samples) and reverse data array
        function prepSamplesData(arr, n){
            if (n > arr.length)
                _.times(n - arr.length, function(){ arr.push(0); });

            return arr.concat().reverse();
        }

    });