#### Description

Implementation for https://bitbucket.org/wellaware/frontend_development_test

##### Setting up your env

NodeJS
http://nodejs.org/download/

Gulp
`$ npm install gulp -g`

After cloning repository install local dependencies
`$ npm install`

##### Running app locally

`$ gulp run`

##### Running app in dev mode

`$ gulp`
