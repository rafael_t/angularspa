/* jshint devel:true, -W097 */
/* global require, __dirname */

'use strict';

var gulp = require('gulp'),
    gopen = require('gulp-open'),
    tinylr = require('tiny-lr')(),
    path = require('path'),

    express = require('express'),
    app = express(),

    appPort = 4000,
    tinylrPort = 4001,
    url = 'http://localhost:' + appPort,

    lrWatch = [
        './index.html',
        'app/views/**/*.html',
        'app/**/*.js',
        'assets/**/*.js',
        'assets/**/*.css'
    ];

gulp.task('livereload', function() {
    tinylr.listen(tinylrPort);
});

gulp.task('express-dev', function() {
    app.use(require('connect-livereload')({
        port: tinylrPort
    }));

    app.use(express.static(__dirname));
    app.listen(appPort);
    console.log('Listening on http://localhost:' + appPort);
});

gulp.task('express', function() {
    app.use(express.static(__dirname));
    app.listen(appPort);
    console.log('Listening on http://localhost:' + appPort);
});

gulp.task('watch', function() {
    gulp.watch(lrWatch, function(e) {
        notifyLiveReload(e);
    });
});

gulp.task('launch', function() {
    var options = {
        url: url,
        app: 'chrome'
    };
    gulp.src('./index.html').pipe(gopen('', options));
});

gulp.task('run', ['express', 'launch']);

gulp.task('dev', ['express-dev', 'livereload', 'launch', 'watch']);

gulp.task('default', ['dev']);

function notifyLiveReload(e) {
    var fileName = path.relative(__dirname, e.path);

    tinylr.changed({
        body: {
            files: [fileName]
        }
    });
}